local Wave = require "model.wave"
-- local Unit = require 'model.unit'
local Vec = require "common.vec"
local Cursor = require "view.cursor"
local SpriteAtlas = require "view.sprite_atlas"
local BattleField = require "view.battlefield"
local Stats = require "view.stats"
local State = require "state"

local PlayStageState = require "common.class"(State)

function PlayStageState:_init(stack)
    self:super(stack)
    self.stage = nil
    self.atlas = nil
    self.battlefield = nil
    self.b_cursor = nil
    self.units = nil
    self.wave = nil
    self.stats = nil
    self.monsters = nil
end

function PlayStageState:enter(params)
    self.re = params.rule_engine
    self:_load_view()

    self.stage = self.re:new_stage(params.stage, self.atlas, self.re, self.battlefield)
    self:_create_unit("capital", self.battlefield:tile_to_screen(-6, 6))
end

function PlayStageState:leave()
    self:view("bg"):remove("battlefield")
    self:view("fg"):remove("atlas")
    self:view("bg"):remove("cursor")
    self:view("hud"):remove("stats")
end

function PlayStageState:_load_view()
    self.battlefield = BattleField()
    self.atlas = SpriteAtlas()
    self.b_cursor = Cursor(self.battlefield)
    local _, right, top, _ = self.battlefield:get_bounds()
    self.stats = Stats(Vec(right + 16, top))
    self:view("bg"):add("battlefield", self.battlefield)
    self:view("fg"):add("atlas", self.atlas)
    self:view("bg"):add("cursor", self.b_cursor)
    self:view("hud"):add("stats", self.stats)
end

function PlayStageState:_create_unit(specname, pos)
    local character = self.re:new_unit(specname, pos, self.atlas)
    return character
end

function PlayStageState:_create_enemy(specname, pos)
    local character = self.re:new_enemy(specname, pos, self.atlas)
    return character
end

function PlayStageState:on_mousepressed(_, _, button)
    if button == 1 then
        local pos = self.b_cursor:get_position()
        if pos then
            self.stage:create_unit("warrior", pos)
        end
    end
end

function PlayStageState:update(dt)
    self.stage:update(dt)
end

return PlayStageState
