return {
    name = "Warrior Troop",
    appearance = "knight",
    max_hp = 20,
    cost = 15,
    power = 6,
    resistance = 4,
    range = 1
}
