return {
    name = "Capital",
    appearance = 'citadel',
    max_hp = 500,
    power = 4,
    resistance = 5,
    range = 2
}

