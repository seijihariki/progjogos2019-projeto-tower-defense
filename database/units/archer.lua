return {
    name = "Archer",
    appearance = "archer",
    max_hp = 12,
    cost = 15,
    power = 4,
    resistance = 3,
    range = 2
}
