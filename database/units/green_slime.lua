return {
    name = "Green Slime",
    appearance = 'slime',
    max_hp = 6,
    power = 3,
    resistance = 2,
    range = 1
}

