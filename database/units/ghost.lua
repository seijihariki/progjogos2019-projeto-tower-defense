return {
    name = "Ghost",
    appearance = 'ghost',
    max_hp = 20,
    power = 4,
    resistance = 4,
    range = 2
}
