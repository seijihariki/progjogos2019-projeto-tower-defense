return {
    name = "Fearless Queen",
    appearance = "queen",
    max_hp = 25,
    cost = 30,
    power = 7,
    resistance = 5,
    range = 1
}
