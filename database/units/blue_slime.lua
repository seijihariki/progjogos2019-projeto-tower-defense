return {
    name = "Blue Slime",
    appearance = 'blue_slime',
    max_hp = 16,
    power = 4,
    resistance = 3,
    range = 1
}
