return {
    name = "Undying Chicken",
    appearance = 'chicken',
    max_hp = 1000,
    power = 50,
    resistance = 50,
    range = 2
}
