return {
  title = "Slime Infestation",
  capital = {0, 0},
  waves = {
    {timer = 5, stages = {{type = "green_slime", count = 10, timer = 1}}},
    {
      timer = 5,
      stages = {{type = "green_slime", count = 10, timer = 0.75}, {type = "blue_slime", count = 5, timer = 2}}
    },
    {
      timer = 5,
      stages = {{type = "green_slime", count = 20, timer = 1}, {type = "blue_slime", count = 10, timer = 1.5}}
    }
  }
}
