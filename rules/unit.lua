return function(ruleset)
    local r = ruleset.record

    r:new_property("unit", {})

    function ruleset.define:new_unit(module, position, atlas) -- receives the character table from database
        function self.when()
            return true
        end
        function self.apply()
            local e = ruleset:new_entity()

            e:use_character(module, position, atlas)

            r:set(e, "unit", {}) -- This is a unit

            return e
        end
    end

    function ruleset.define:update(e, dt)
        function self.when()
            return r:is(e, "unit")
        end
        function self.apply()
            for _, enemy in ipairs(r:all("enemy")) do
                e:attack(enemy, dt)
            end
        end
    end
end
