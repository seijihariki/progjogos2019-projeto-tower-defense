return function(ruleset)
  local r = ruleset.record

  r:new_property("money", {amount = 0})

  function ruleset.define:get_money(e)
    function self.when()
      return r:is(e, "money")
    end
    function self.apply()
      return r:get(e, "money", "amount")
    end
  end

  function ruleset.define:is_dead(e)
    function self.when()
      return r:is(e, "creature")
    end
    function self.apply()
      return e.toughness <= 0
    end
  end

  function ruleset.define:get_description(e)
    function self.when()
      return r:is(e, "creature")
    end
    function self.apply()
      return ("%s (%d/%d creature)"):format(e.name, e.power, e.toughness)
    end
  end
end
