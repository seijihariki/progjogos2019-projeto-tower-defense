local Vec = require "common.vec"

return function(ruleset)
    local r = ruleset.record

    r:new_property("enemy", {target = Vec()})

    function ruleset.define:new_enemy(module, position, atlas, target) -- receives the character table from database
        function self.when()
            return true
        end
        function self.apply()
            local e = ruleset:new_entity()

            e:use_character(module, position, atlas)

            r:set(
                e,
                "enemy",
                {
                    target = target
                }
            ) -- This is an enemy

            return e
        end
    end

    function ruleset.define:update(e, dt)
        function self.when()
            return r:is(e, "enemy")
        end
        function self.apply()
            e:move(e.position + (r:get(e, "enemy", "target") - e.position):normalized() * 20 * dt)

            for _, unit in ipairs(r:all("unit")) do
                e:attack(unit, dt)
            end
        end
    end
end
