local Vec = require "common.vec"

return function(ruleset)
    local r = ruleset.record

    r:new_property(
        "character",
        {
            name = "name",
            appearance = "appearance",
            max_hp = 1,
            power = 1,
            resistance = 1,
            range = 1,
            current_hp = 1,
            position = Vec(),
            atlas = {}
        }
    )

    function ruleset.define:use_character(e, specname, position, atlas) -- Applies character property on entity
        function self.when()
            return true
        end
        function self.apply()
            assert(atlas ~= nil, "Atlas must not be nil")
            assert(position.x and position.y, "Atlas must be a Vec")

            r:set(e, "character", require("database.units." .. specname)) -- set all character stats at once
            r:set(e, "character", {current_hp = e.max_hp}) -- initializes hp as max_hp
            r:set(e, "character", {position = position, atlas = atlas})

            atlas:add(e, position, e.appearance)

            return e
        end
    end

    function ruleset.define:get_max_hp(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "max_hp")
        end
    end

    function ruleset.define:get_hp_percentage(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return math.max(e.hp, 0) / e.max_hp
        end
    end

    function ruleset.define:get_hp(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "current_hp")
        end
    end

    function ruleset.define:set_hp(e, new_hp)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:set(e, "character", "current_hp", new_hp)
        end
    end

    function ruleset.define:get_power(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "power")
        end
    end

    function ruleset.define:get_resistance(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "resistance")
        end
    end

    function ruleset.define:get_range(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "range")
        end
    end

    function ruleset.define:get_position(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "position")
        end
    end

    function ruleset.define:get_dead(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return e.hp <= 0
        end
    end

    function ruleset.define:get_description(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return ("%s (power: %d / resistance: %d / range: %d)"):format(e.name, e.power, e.resistance, e.range)
        end
    end

    function ruleset.define:get_appearance(e)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            return r:get(e, "character", "appearance")
        end
    end

    function ruleset.define:move(e, pos)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            r:set(e, "character", "position", pos)
            r:get(e, "character", "atlas"):get(e).position = pos
        end
    end

    function ruleset.define:destroy(e) -- most prioritary destroy feature
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            if e.dead then
                local atlas = r:get(e, "character", "atlas")
                atlas:remove(e)
                r:clear_all(e)
                return true
            end

            return false
        end
    end

    function ruleset.define:in_range(e, character)
        function self.when()
            return r:is(e, "character") and r:is(character, "character")
        end
        function self.apply()
            local radius = 32 * e.range -- tile size * range
            local pos_char = e.position
            local pos_other = character.position

            return (pos_char - pos_other):length() <= radius
        end
    end
end
