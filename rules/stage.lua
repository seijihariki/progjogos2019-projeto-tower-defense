return function(ruleset)
  local r = ruleset.record

  r:new_property(
    "stage",
    {
      -- Waves
      waves = {},
      -- Generic timer
      timer = 0,
      -- If true, wave spawning. If false, wave in wait
      wave_started = false,
      -- If is paused
      paused = false,
      -- Atlas
      atlas = {},
      -- Engine
      engine = {},
      -- BattleField
      battlefield = {}
    }
  )

  -- Getters and Setters

  -- Engine
  function ruleset.define:get_engine(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "engine")
    end
  end

  -- Atlas
  function ruleset.define:get_atlas(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "atlas")
    end
  end

  -- Battlefield
  function ruleset.define:get_battlefield(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "battlefield")
    end
  end

  -- Current Wave
  function ruleset.define:get_current_wave(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "waves")[1]
    end
  end

  -- Timer
  function ruleset.define:get_timer(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "timer")
    end
  end

  -- Paused
  function ruleset.define:get_paused(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return r:get(e, "stage", "paused")
    end
  end

  -- Creates a new stage from stage object
  function ruleset.define:new_stage(stage, atlas, engine, battlefield)
    function self.when()
      return true
    end
    function self.apply()
      assert(atlas ~= nil, "Atlas must not be nil")
      assert(engine ~= nil, "Engine must not be nil")
      assert(battlefield ~= nil, "Battlefield must not be nil")

      local e = ruleset:new_entity()
      r:set(e, "stage", stage)
      r:set(e, "stage", "timer", 5)
      r:set(
        e,
        "stage",
        {
          atlas = atlas,
          engine = engine,
          battlefield = battlefield
        }
      )
      r:set(e, "money", {amount = 1000})
      return e
    end
  end

  -- Updates stage
  function ruleset.define:update(e, dt)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      -- Don't do anything if paused
      if e.paused then
        return
      end

      -- Update timer
      if e.timer > 0 then
        r:set(e, "stage", "timer", math.max(e.timer - dt, 0))
      end

      local wave_started = r:get(e, "stage", "wave_started")

      -- Wave start wait logic
      if (not wave_started) and e.timer == 0 then
        r:set(e, "stage", "wave_started", true)
      end

      -- Wave spawn logic
      if wave_started and e.timer == 0 then
        e:spawn_enemy()
      end

      -- Updates characters
      for _, character in ipairs(r:all("character")) do
        character:update(dt)
      end
    end
  end

  -- Spawns an enemy
  function ruleset.define:spawn_enemy(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      if #e.current_wave.stages < 1 then
        if #r:all("enemy") < 1 then
          e:next_wave()
        end

        return
      end

      if e.current_wave.stages[1].count == 0 then
        table.remove(e.current_wave.stages, 1)
        return
      end

      local spawn_location = e.battlefield:tile_to_screen(math.random(5, 7), math.random(-7, -5))

      e.engine:new_enemy(e.current_wave.stages[1].type, spawn_location, e.atlas, e.battlefield:tile_to_screen(-6, 6))
      r:set(e, "stage", "timer", e.current_wave.stages[1].timer)
      e.current_wave.stages[1].count = e.current_wave.stages[1].count - 1
    end
  end

  -- Create unit
  function ruleset.define:create_unit(e, specname, coords)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      return e.engine:new_unit(specname, coords, e.atlas)
    end
  end

  -- Goes to next wave
  function ruleset.define:next_wave(e)
    function self.when()
      return r:is(e, "stage")
    end
    function self.apply()
      -- Go to next wave
      table.remove(r:get(e, "stage", "waves"), 1)

      -- Set timer
      r:set(e, "stage", "timer", e.current_wave.timer)

      -- Reset wave start
      r:set(e, "stage", "wave_started", false)
    end
  end
end
