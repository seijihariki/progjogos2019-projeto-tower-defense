return function(ruleset)
    local r = ruleset.record

    function ruleset.define:attack(e, _) -- runs in case is not in range or target is nil
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
        end
    end

    function ruleset.define:attack(e, character, dt) -- most prioritary attack
        function self.when()
            return r:is(e, "character") and r:is(character, "character") and e:in_range(character)
        end
        function self.apply()
            local damage = e.power + math.random(-1, 1)
            character:take_dmg(damage, dt)
        end
    end

    function ruleset.define:take_dmg(e, damage, dt)
        function self.when()
            return r:is(e, "character")
        end
        function self.apply()
            local resist = e.resistance + math.random(-1, 1)
            local dmg = math.floor(damage - resist / 2)
            e.hp = e.hp - dmg * dt

            e:destroy()
        end
    end
end
