local SelectableGrid = require "view.selectable_grid"
local Vec = require "common.vec"
local UnitSelector = require "common.class"()

function UnitSelector:_init()
  local h = love.graphics.getHeight()
  local center = Vec(1, 1) * h / 2 + Vec(32 * 11, 32 * 2)

  self.grid = SelectableGrid(2, 6, center)
end

function UnitSelector:center()
  return self.grid:center()
end

function UnitSelector:to_tile(pos)
  return self.grid:to_tile(pos)
end

function UnitSelector:round_to_tile(pos)
  return self.grid:round_to_tile(pos)
end

function UnitSelector:tile_to_screen(x, y)
  return self.grid:tile_to_screen(x, y)
end

function UnitSelector:draw()
  self.grid:draw()
end

function UnitSelector:get_bounds()
  return self.grid:get_bounds()
end

return UnitSelector
