local SelectableGrid = require "view.selectable_grid"
local Vec = require "common.vec"
local BattleField = require "common.class"()

function BattleField:_init()
  local h = love.graphics.getHeight()
  local center = Vec(1, 1) * h / 2

  self.grid = SelectableGrid(8, 8, center)
end

function BattleField:center()
  return self.grid:center()
end

function BattleField:round_to_tile(pos)
  return self.grid:round_to_tile(pos)
end

function BattleField:tile_to_screen(x, y)
  return self.grid:tile_to_screen(x, y)
end

function BattleField:draw()
  self.grid:draw()
end

function BattleField:get_bounds()
  return self.grid:get_bounds()
end

return BattleField
