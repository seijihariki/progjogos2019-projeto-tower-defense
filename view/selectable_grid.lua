local Box = require "common.box"
local Vec = require "common.vec"
local SelectableGrid = require "common.class"()

function SelectableGrid:_init(width, height, center, tile_size)
  self.tile_size = tile_size or 32
  self.size = Vec(width, height)
  self.bounds = Box.from_vec(center, self.size * self.tile_size)
  print(self.bounds)
end

function SelectableGrid:center()
  local b = self.bounds
  return (Vec(b.left, b.top) + Vec(b.right, b.bottom)) / 2
end

function SelectableGrid:to_tile(pos)
  local center = self:center()
  local dist = pos - center
  local tile = ((dist + Vec(1, 1) * self.tile_size / 2) / self.tile_size):floor()

  if math.abs(tile.x) >= self.size.x or math.abs(tile.y) >= self.size.y then
    return nil
  end

  return tile
end

function SelectableGrid:round_to_tile(pos)
  local center = self:center()
  local dist = pos - center
  local tile = ((dist + Vec(1, 1) * self.tile_size / 2) / self.tile_size):floor()

  if math.abs(tile.x) >= self.size.x or math.abs(tile.y) >= self.size.y then
    return nil
  end

  return center + tile * self.tile_size
end

function SelectableGrid:tile_to_screen(x, y)
  local center = self:center()
  return center + Vec(x, y):floor() * self.tile_size
end

function SelectableGrid:draw()
  local g = love.graphics
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle("line", self.bounds:get_rectangle())
end

function SelectableGrid:get_bounds()
  return self.bounds:get()
end

return SelectableGrid
