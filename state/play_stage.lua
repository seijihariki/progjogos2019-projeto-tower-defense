-- local Unit = require 'model.unit'
local Vec = require "common.vec"
local Cursor = require "view.cursor"
local SpriteAtlas = require "view.sprite_atlas"
local BattleField = require "view.battlefield"
local UnitSelector = require "view.unit_selector"
local Stats = require "view.stats"
local State = require "state"

local PlayStageState = require "common.class"(State)

function PlayStageState:_init(stack)
    self:super(stack)
    self.stage = nil
    self.atlas = nil
    self.battlefield = nil
    self.b_cursor = nil
    self.units = nil
    self.stats = nil
    self.monsters = nil
    self.selected = "warrior"
end

function PlayStageState:enter(params)
    self.re = params.rule_engine
    self:_load_view()

    self.stage = self.re:new_stage(params.stage, self.atlas, self.re, self.battlefield)
    self:_create_unit("capital", self.battlefield:tile_to_screen(-6, 6))

    -- Populate selector
    self:_create_unit("warrior", self.unit_selector:tile_to_screen(-1, -5))
    self:_create_unit("archer", self.unit_selector:tile_to_screen(0, -5))
    self:_create_unit("queen", self.unit_selector:tile_to_screen(1, -5))
end

function PlayStageState:leave()
    self:view("bg"):remove("battlefield")
    self:view("bg"):remove("unit_selector")
    self:view("fg"):remove("atlas")
    self:view("bg"):remove("b_cursor")
    self:view("bg"):remove("s_cursor")
    self:view("hud"):remove("stats")
end

function PlayStageState:_load_view()
    self.battlefield = BattleField()
    self.unit_selector = UnitSelector()
    self.atlas = SpriteAtlas(self.re)
    self.b_cursor = Cursor(self.battlefield)
    self.s_cursor = Cursor(self.unit_selector)
    local _, right, top, _ = self.battlefield:get_bounds()
    self.stats = Stats(Vec(right + 16, top))
    self:view("bg"):add("battlefield", self.battlefield)
    self:view("bg"):add("unit_selector", self.unit_selector)
    self:view("fg"):add("atlas", self.atlas)
    self:view("bg"):add("b_cursor", self.b_cursor)
    self:view("bg"):add("s_cursor", self.s_cursor)
    self:view("hud"):add("stats", self.stats)
end

function PlayStageState:_create_unit(specname, pos)
    local character = self.re:new_unit(specname, pos, self.atlas)
    return character
end

function PlayStageState:_create_enemy(specname, pos)
    local character = self.re:new_enemy(specname, pos, self.atlas)
    return character
end

function PlayStageState:on_mousepressed(_, _, button)
    if button == 1 then
        local pos = self.b_cursor:get_position()
        if pos then
            self.stage:create_unit(self.selected, pos)

            local sominsert = love.audio.newSource("sons/inserting.ogg", "static")
            sominsert:play()
        end

        local spos = self.s_cursor:get_position()
        if spos then
            spos = self.unit_selector:to_tile(spos)

            if spos.x == -1 and spos.y == -5 then
                self.selected = "warrior"
            end
            if spos.x == 0 and spos.y == -5 then
                self.selected = "archer"
            end
            if spos.x == 1 and spos.y == -5 then
                self.selected = "queen"
            end
        end
    end
end

function PlayStageState:update(dt)
    self.stage:update(dt)
end

return PlayStageState
